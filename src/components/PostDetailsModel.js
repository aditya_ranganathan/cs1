import React from 'react';
import {Modal} from 'react-bootstrap';
import PostDetails from './PostDetails';

const PostDetailsModel = (props) => {

    const {show, onHide, posts, deletePost, id, addComment, deleteComment, likeCount, editInfo} = props 
    return (
        <Modal 
          show={show} 
          onHide={onHide}
          bsSize="large"
          aria-labelledby="contained-modal-title-lg"
          dialogClassName="model-style"
        >
            <Modal.Body>
                <PostDetails
                    id={id}
                    posts={posts}
                    deletePost={deletePost}
                    likeCount={likeCount}
                    addComment={addComment}
                    editInfo={editInfo}
                    deleteComment={deleteComment}/>
            
            </Modal.Body>
        </Modal>
      );
    }


 export default PostDetailsModel 