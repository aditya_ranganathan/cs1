
Extract the .zip file
Navigate to extracted directory through terminal
Run => npm install (to install the project dependencies)
Run => npm start (ro run the application)

In chrome enable cross-origin resource sharing (install any plugin to enable CORS)

Features Implemented =>
1) Upload Post By selecting Image file and clicking on Upload Button
2) On Overlay Image shows #likes and #comments
3) In Profile Info region showing #posts
4) Clicking on Image opens Carousel where we can navigate next or previous post
5) Carousel contains two parts. left part shows Image and right part shows Details about Post
6) In Details Part we can Delete Post by clicking on Delete icon Button
7) We can Add add Comments By entering into Comment input and by pressing Enter.
8) We can Delete comments.
9) Like/Unlike Post by clicking on Like button (color changes to red for Like as well as count increments) 
10) We can add Label to Post by clicking on Plus Icon button. If already label present then we can edit by clicking on Pencil Icon button.
11) #like and #comment reflected to image overlay after changing in Details section.

